# distro_initial_setup
---

Scripts for configuring any brand new Linux installation

### ToDo
- Learn loops in `bash`
- Copy `.config` dir to `~/.config`
- get your feet wet with TUI (whiptail or tput)
  - https://thecustomizewindows.com/2016/12/build-colorful-command-line-tui-shell-scripts/

- git.sh
- code-oss (FOSS alternative for VScode)
- rust development environment
  - rustup
  - cargo
  - code-oss extensions (`vscode --install-extension <extension-id>`)
  - `code-oss --install-extension vadimcn.vscode-lldb serayuzgur.crates webfreak.debug rust-lang.rust hdevalke.rust-test-lens matklad.rust-analyzer be5invis.toml`
- micro
- gedit
  - frontmatter for put_it_out_there_zola theme
- setup appimaged
  - move AppImages to ~/Applications
    - bitwarden
    - gimp
    - lagrange (gemini client)
    - mattermost
    - nextcloud
    - openscad
- starship prompt for bash
